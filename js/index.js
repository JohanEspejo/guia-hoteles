	$(function () {
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();
  			$('.carousel').carousel(
  			{
  				interval:2000
  			});

  			$('#ModalContacto').on('show.bs.modal', function (e)
  			{
  				console.log('El modal se esta mostrando');

  				$('#btnContacto').removeClass('btn-outline-info');
  				$('#btnContacto').addClass('btn-primary');
  				$('#btnContacto').prop('disabled',true);


  			});
  			$('#ModalContacto').on('shown.bs.modal', function (e)
  			{
  				console.log('El modal se mostro');
  			});
  			$('#ModalContacto').on('hide.bs.modal', function (e)
  			{
  				console.log('El modal se cerro');
  				

  			});
  			$('#ModalContacto').on('hidden.bs.modal', function (e)
  			{
  				console.log('El modal esta cerrado');
  				$('#btnContacto').prop('disabled',false);
  				$('#btnContacto').removeClass('btn-primary');
  				$('#btnContacto').addClass('btn-outline-info');

  			});

		})
